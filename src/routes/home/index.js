import { h, Component } from "preact";
import style from "./style";

const BACKGROUND_KEY = "__background-transition-ts__";
const FOREGROUND_KEY = "__foreground-transition-ts__";
const HIDEPAGE_KEY = "__hidepage__";

class Home extends Component {
  mql = window.matchMedia("(orientation: portrait)");
  state = {
    visible: true,
    isPortrait: this.mql.matches,
  };

  request = () => {
    return fetch("https://jsonplaceholder.typicode.com/posts/1")
      .then((response) => response.json())
      .then((json) => console.log(json));
  };

  onVisibilityChange = () => {
    const state = document.visibilityState;
    if (state === "hidden") {
      this.request().then(() => {
        if (!this.mounted) return;
        localStorage.setItem(BACKGROUND_KEY, new Date().toISOString());
        this.forceUpdate();
      });
    }

    if (state === "visible") {
      this.request().then(() => {
        if (!this.mounted) return;
        localStorage.setItem(FOREGROUND_KEY, new Date().toISOString());
        this.forceUpdate();
      });
    }
  };

  onOrientationChange = (m) => {
    console.log(m.matches);
    this.setState({ isPortrait: m.matches }, () => {
      console.log(this.state);
    });
  };

  componentDidMount() {
    this.mounted = true;
    document.addEventListener("visibilitychange", this.onVisibilityChange);
    if (this.mql != null) {
      if (typeof this.mql.addEventListener === "function") {
        this.mql.addEventListener("change", this.onOrientationChange);
      } else if (typeof this.mql.addListener === "function") {
        this.mql.addListener(this.onOrientationChange);
      }
    }
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener("visibilitychange", this.onVisibilityChange);
    if (this.mql != null) {
      console.log(1);
      if (typeof this.mql.removeEventListener === "function") {
        console.log(11);
        this.mql.removeEventListener("change", this.onOrientationChange);
      } else if (typeof this.mql.removeListener === "function") {
        this.mql.removeListener(this.onOrientationChange);
      }
      this.mql = null;
    }
  }

  render() {
    const bg = localStorage.getItem(BACKGROUND_KEY);
    const fg = localStorage.getItem(FOREGROUND_KEY);
    const hp = localStorage.getItem(HIDEPAGE_KEY);

    return (
      <div class={style.home}>
        <p>background: {bg ? bg : "-"}</p>
        <p>foreground: {fg ? fg : "-"}</p>
        {/* <p>hidepage: {hp ? hp : "-"}</p> */}
        <p>orientation: {this.state.isPortrait ? "portrait" : "landscape"}</p>
      </div>
    );
  }
}

export default Home;
